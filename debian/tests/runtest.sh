#!/bin/sh

set -e

# Taken from https://github.com/odedshimon/BruteShark/issues/124
# Unfortunately brutesharkcli returns 0 on failure, and it writes
# errors to stdout, so we need grep.
brutesharkcli \
  -i /usr/share/doc/bruteshark/examples/Pcap_Examples/Ftp.pcap \
  -m Credentials -o Example \
  | grep ^ERROR: && exit 1 || :
